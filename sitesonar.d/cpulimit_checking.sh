#!/bin/bash

# Checks if it is running in a cgroup cpu limiter

VAR=`grep cpu,cpuacct /proc/$$/cgroup`

exitcode=$?

if [ $exitcode -ne 0 ]; then
	VAR=`grep cpuacct,cpu /proc/$$/cgroup`
	exitcode=$?
	if [ $exitcode -ne 0 ]; then
		JSON_OUTPUT="{ \"ACCOUNTING\" : \"\", \"CGROUP\" : \"\", \"ACCESS_QUOTA\" : \"\", \"ACCESS_PERIOD\" : \"\", \"ALLOCATED_CPUS\" : \"\" }"
		echo "$JSON_OUTPUT"
		exit $exitcode
	fi
fi

accounting=`echo ${VAR} | cut -d':' -f3`
#echo $accounting

JSON_OUTPUT="{ \"ACCOUNTING\" : \"$accounting\" "

cpu_cgroup=$(dirname $(find /sys/fs/ -name "*.procs" -exec grep ^$$\$ {} /dev/null \; 2> /dev/null | grep cpu,cpuacct))
if [[ -z $cpu_cgroup ]]; then
	JSON_OUTPUT+=", \"CGROUP\" : \"\", \"ACCESS_QUOTA\" : \"\", \"ACCESS_PERIOD\" : \"\", \"ALLOCATED_CPUS\" : \"\" }"
	echo "$JSON_OUTPUT"
	exit 1
fi
JSON_OUTPUT+=", \"CGROUP\" : \"$cpu_cgroup\" "

quota=$(cat $cpu_cgroup/cpu.cfs_quota_us)
if [[ $quota -lt 0 ]]; then
	JSON_OUTPUT+=", \"ACCESS_QUOTA\" : \"\", \"ACCESS_PERIOD\" : \"\", \"ALLOCATED_CPUS\" : \"\" }"
	echo "$JSON_OUTPUT"
	exit 2
fi

JSON_OUTPUT+=", \"ACCESS_QUOTA\" : \"$quota\" "

period=$(cat $cpu_cgroup/cpu.cfs_period_us)

JSON_OUTPUT+=", \"ACCESS_PERIOD\" : \"$period\" "

#echo $(($quota * 100 / $period))

JSON_OUTPUT+=", \"ALLOCATED_CPUS\" : \"$(($quota * 100 / $period))\" } "

echo "$JSON_OUTPUT"
