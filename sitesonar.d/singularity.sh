#!/bin/bash

# Print whether singularity is supported

SINGULARITY_EXEC="singularity exec -B /cvmfs:/cvmfs /cvmfs/alice.cern.ch/containers/fs/singularity/centos7 java -version"

CHECK_LOCAL=$($SINGULARITY_EXEC 2>&1 | grep -o "Runtime")
CHECK_CVMFS=$(/cvmfs/alice.cern.ch/containers/bin/singularity/current/bin/$SINGULARITY_EXEC 2>&1 | grep -o "Runtime")

if [ -z "$CHECK_LOCAL" ]
  then
      SINGULARITY_LOCAL_SUPPORTED=false
  else
      SINGULARITY_LOCAL_SUPPORTED=true
fi

if [ -z "$CHECK_CVMFS" ]
  then
      SINGULARITY_CVMFS_SUPPORTED=false
  else
      SINGULARITY_CVMFS_SUPPORTED=true
fi

echo "{ \"SINGULARITY_LOCAL_SUPPORTED\" : $SINGULARITY_LOCAL_SUPPORTED, \"SINGULARITY_CVMFS_SUPPORTED\" : $SINGULARITY_CVMFS_SUPPORTED }"
