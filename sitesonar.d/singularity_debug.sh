#!/bin/bash

# Print whether singularity is supported

df_seccomp=$( (df -P /lib64/libseccomp.so.2 | awk 'NR > 1') 2>&1 )

SINGULARITY_EXEC="singularity exec -B /cvmfs:/cvmfs /cvmfs/alice.cern.ch/containers/fs/singularity/centos7 java -version"

LOCAL_OUTPUT=$($SINGULARITY_EXEC 2>&1)
CVMFS_OUTPUT=$(/cvmfs/alice.cern.ch/containers/bin/singularity/current/bin/$SINGULARITY_EXEC 2>&1)

my_transform()
{
    perl -p0777e 's/(.{995}).{6,}/$1\[...]/s' | tr -c '[\t\r\n -~]' \? | perl -pe '
	s/\n+$//;
	s/[\\"]/\\$&/g;
	s/\t/\\t/g;
	s/\r/\\r/g;
	s/\n/\\n/g;
    '
}

CHECK_SECCOMP=$(echo "$df_seccomp" | my_transform)

CHECK_LOCAL_DEBUG=$(echo "$LOCAL_OUTPUT" | my_transform)
CHECK_CVMFS_DEBUG=$(echo "$CVMFS_OUTPUT" | my_transform)

CHECK_LOCAL=$(echo "$LOCAL_OUTPUT" | grep -o "Runtime")
CHECK_CVMFS=$(echo "$CVMFS_OUTPUT" | grep -o "Runtime")

if [ -z "$CHECK_LOCAL" ]
  then
      SINGULARITY_LOCAL_SUPPORTED=false
  else
      SINGULARITY_LOCAL_SUPPORTED=true
fi

if [ -z "$CHECK_CVMFS" ]
  then
      SINGULARITY_CVMFS_SUPPORTED=false
  else
      SINGULARITY_CVMFS_SUPPORTED=true
fi

output=
output+="{ \"SINGULARITY_LOCAL_SUPPORTED_BOOL\" : $SINGULARITY_LOCAL_SUPPORTED"
output+=", \"SINGULARITY_CVMFS_SUPPORTED_BOOL\" : $SINGULARITY_CVMFS_SUPPORTED"
output+=", \"SINGULARITY_LOCAL_DEBUG\"          : \"$CHECK_LOCAL_DEBUG\""
output+=", \"SINGULARITY_CVMFS_DEBUG\"          : \"$CHECK_CVMFS_DEBUG\""
output+=", \"SINGULARITY_CHECK_SECCOMP\"        : \"$CHECK_SECCOMP\""
output+="}"

echo "$output"