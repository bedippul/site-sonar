#!/bin/bash

# Print the redhat version

OS_DETAILS=$(cat /etc/os-release)

JSON_OUTPUT="{ "
while IFS= read -r line ; 
    do 
    if [ ! -z "$line" ]; then
        key=$(echo "$line" | cut -d "=" -f 1)
        val=$(echo "$line" | cut -d "=" -f 2- | xargs)
        JSON_OUTPUT+="\"OS_$key\" : \"$val\","; 
    fi
done <<< "${OS_DETAILS}"
JSON_OUTPUT+=" }"

echo $JSON_OUTPUT | rev | sed '1 s/,//' | rev # remove trailing comma in json