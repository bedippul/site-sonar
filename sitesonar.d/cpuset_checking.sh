#!/bin/bash

# Checks if it is running in a cpuset

VAR=`grep cpuset /proc/$$/cgroup`

exitcode=$?

if [ $exitcode -ne 0 ]; then
    JSON_OUTPUT="{ \"CPUSET_ENABLED\" : false, \"CPUSET_PREFIX\" : \"\", \"CPUSET_CGROUP\" : \"\", \"CPUSET_CPUS\" : \"\" , \"CPU_AMOUNT\" : \"\" }"
    echo "$JSON_OUTPUT"
    exit $exitcode
fi

JSON_OUTPUT="{ \"CPUSET_ENABLED\" : true "

prefix=`echo ${VAR} | cut -d':' -f3 | cut -d'/' -f2 |  (echo -n / && cat)`
#echo $prefix

JSON_OUTPUT+=", \"CPUSET_PREFIX\" : \"$prefix\" "

cpu_cgroup=$(dirname $(find /sys/fs/ -name "*.procs" -exec grep ^$$\$ {} /dev/null \; 2> /dev/null | grep cpuset))
if [[ -z $cpu_cgroup ]]; then
	JSON_OUTPUT+=", \"CPUSET_CGROUP\" : \"\", \"CPUSET_CPUS\" : \"\", \"CPU_AMOUNT\" : \"\" }"
	echo "$JSON_OUTPUT"
	exit 1
fi

JSON_OUTPUT+=", \"CPUSET_CGROUP\" : \"$cpu_cgroup\" "

cpuset=$(cat $cpu_cgroup/cpuset.cpus)
if [[ -z $cpuset ]]; then
	JSON_OUTPUT+=", \"CPUSET_CPUS\" : \"\", \"CPU_AMOUNT\" : \"\" }"
	echo "$JSON_OUTPUT"
	exit 2
fi

JSON_OUTPUT+=", \"CPUSET_CPUS\" : \"$cpuset\" "

nr_cpus=$(cat /proc/cpuinfo | grep processor | wc -l)
JSON_OUTPUT+=", \"CPU_AMOUNT\" : $nr_cpus }"
if [[ "0-$(($nr_cpus - 1))" == $cpuset ]]; then
	echo "$JSON_OUTPUT"
	exit 3
fi

#echo $cpuset

echo "$JSON_OUTPUT"
