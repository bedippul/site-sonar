#!/bin/bash

# Print the maximum namespace

MAX_NAMESPACES=$(cat /proc/sys/user/max_user_namespaces)

if [[ $? == 0 ]]
then
    echo "{ \"MAX_NAMESPACES\" : $MAX_NAMESPACES }"
else 
    exit_code=$?
    echo "{ \"MAX_NAMESPACES\" : \"\" }"
    exit $exit_code
fi
