#! /bin/bash

# Gets the number of cores defined in the JDL file

if [[ -v "$ALIEN_JDL_CPUCORES" ]]
then
    echo "{ \"ALIEN_JDL_CPUCORES\" : $ALIEN_JDL_CPUCORES }"
else
    echo "{ \"ALIEN_JDL_CPUCORES\" : \"\" }"
fi
