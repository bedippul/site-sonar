#!/bin/bash

# Print the LHCB Benchmark score

LHCB_MARK=$(/cvmfs/alice.cern.ch/scripts/lhcbmarks.sh)

key=$(echo "$LHCB_MARK" | cut -d ":" -f 1 | xargs)
val=$(echo "$LHCB_MARK" | cut -d ":" -f 2- | xargs)

if [ ! -z "$key" ]; then
    echo "{ \"$key\" : $val }"
else
    echo '{ "Error": "Cannot run the LHCbMarks test" }'
    exit 1
fi
