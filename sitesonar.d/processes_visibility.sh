#!/bin/bash

# Checks how many users are running processes

count=$(ps -e -o user --no-header  | sort -u | wc -l)

JSON_OUTPUT="{ \"USERS_COUNT\" : $count }"

echo $JSON_OUTPUT
