#!/bin/bash

# Controler script, checking what should be run and executing the respective scripts from sitesonar.d/ in turn

hostname=`hostname -f`

# PARENT_HOSTNAME variable is used at sites which the node hostname changes over time. eg: RAL
hname="${PARENT_HOSTNAME:-$hostname}"

# Birmingham site adds a number at the end of actual hostname. eg: "hostname-25.ph.bham.ac.uk"
# Following filter is added to obtain the actual hostname from the given hostname variable.
SED='s/-[0-9]*\(.ph.bham.ac.uk\)$/\1/'
hname=$(echo $hname | sed "$SED")

tests=$(curl -s "http://alimonitor.cern.ch/sitesonar/query.jsp?hostname=${hname}&ce_name=${ALIEN_SITE}")

echo "===== Running the following tests on ${hname} at ${ALIEN_SITE} ====="
echo "${tests}"

for test_name in $tests
do  
    start_time="$(date -u +%s)"
    output=`/cvmfs/alice.cern.ch/sitesonar/sitesonar.d/${test_name}.sh`
    exitcode=$?
    end_time="$(date -u +%s)"
    execution_time=$((end_time - start_time))

    echo "Exit code of '${test_name}' is ${exitcode}. Execution time is ${execution_time}"
    echo "output : ${output}"
    echo "-------------------------------------------------------------------"

    d=

    case $output in
    *:*)
	d=,
    esac

    # Insert the output minus its last closing curly bracket (and any following whitespace)
    updated_output="${output%\}*}$d \"EXECUTION_TIME\" : ${execution_time}, \"EXITCODE\" : ${exitcode} }"

    curl \
        --data-urlencode "hostname=${hname}" \
        --data-urlencode "ce_name=${ALIEN_SITE}" \
        --data-urlencode "test_name=${test_name}" \
        --data-urlencode "test_code=${exitcode}" \
        --data-urlencode "test_message=${updated_output}" \
        "http://alimonitor.cern.ch/sitesonar/upload.jsp"
done