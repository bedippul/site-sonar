LOGSTASH_HOST="alice-logstash.cern.ch"
LOGSTASH_PORT=5402

OUTPUT_FILE="/home/monalisa/MLrepository/tomcat/webapps/ROOT/download/kalana/site-sonar-`date +%s`.out"

query="WITH data(host_id,hostname,ce_name,addr, last_updated, test_results_json) AS (  
    SELECT
        host_id,hostname,
        ce_name,addr,max(last_updated),
        json_object_agg(test_name, test_message_json)
    FROM
        sitesonar_tests INNER JOIN sitesonar_hosts using(host_id)
    WHERE
        last_updated>extract(epoch from now()-'1 week'::interval)::int
    GROUP BY 
        host_id,hostname,ce_name,addr
    HAVING
        max(last_updated)>extract(epoch from now()-'24 hours'::interval)::int
)
SELECT row_to_json(data) FROM data;"

# Get last chunk of data into temp file
/home/monalisa/MLrepository/scripts/pgsql_query_any.sh "$query" > $OUTPUT_FILE

cat $OUTPUT_FILE | nc $LOGSTASH_HOST $LOGSTASH_PORT -q 1
    
xz $OUTPUT_FILE
